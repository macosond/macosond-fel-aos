/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.aos.db;

import cz.cvut.fel.aos.db.entities.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
/**
 *
 * @author lubos
 */
public class AOSMemoryDBUserQueryset extends ArrayList<User>{

    AOSMemoryDBUserQueryset(Collection<User> values) {
        super(values);
    } 
    
    public AOSMemoryDBUserQueryset orderBy(String name) {
        String direction = "asc";
        if (name != null && name.contains(":")) {
            String [] params = name.split(":", 2);
            direction   = params[1];
            name        = params[0];
        }
        Collections.sort(this, new OrderingComparator(name, direction));
        return this;
    }
    
    class OrderingComparator implements Comparator<User> {

        private String field;
        private String direction;
        
        public OrderingComparator(String field, String direction) {
            this.field = field;
            this.direction = direction;
        }

        public int compare(User o1, User o2) {
            int ord = direction.equals("asc") ? 1 : -1;
            if (this.field != null && this.field.endsWith("nickname")){
                return o1.getNick().compareTo(o2.getNick()) * ord;
            }
            return 0;
         }
        
    }
}
