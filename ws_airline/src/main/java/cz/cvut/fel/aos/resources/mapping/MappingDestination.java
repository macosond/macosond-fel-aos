package cz.cvut.fel.aos.resources.mapping;

import cz.cvut.fel.aos.db.entities.*;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
@XmlRootElement(name="destination")
public class MappingDestination {

    private long id;
    private String name;
    private double latitude;
    private double longitude;
    private String uri;

    public MappingDestination() {}

    public MappingDestination(Destination destination) {
        if (destination != null) {
            this.id = destination.getId();
            this.name = destination.getName();
            this.latitude = destination.getLatitude();
            this.longitude = destination.getLongitude();
        }
    }

    public MappingDestination(Destination destination, UriBuilder ub) {
        this(destination);
        this.uri = ub.path(""+this.id).build().getPath();
    }

    @XmlElement(required=true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlElement(required=true)
    public String getName() {
        return name;
    }

    public void setName(String n) {
        name = n;
    }

    @XmlElement(name = "lat")
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double l) {
        latitude = l;
    }

    @XmlElement(name = "lon")
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double l) {
        longitude = l;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return this.uri;
    }

}
