package cz.cvut.fel.aos.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class AccessDeniedRequestException extends WebApplicationException {

    private static final long serialVersionUID = 1L;
    private String error;


    public AccessDeniedRequestException(String error)
    {
        super(Response.status(Response.Status.FORBIDDEN).type(MediaType.TEXT_PLAIN)
                .entity(error).build());
        this.error = error;
    }

    public String getError()
    {
        return error;
    }
}
