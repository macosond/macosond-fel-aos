package cz.cvut.fel.aos.services.printer;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import java.io.*;

public class FileUtils {

    // source http://stackoverflow.com/questions/2836646/java-serializable-object-to-byte-array
    public static byte[] object2byte (Object obj) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        byte[] yourBytes;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            yourBytes = bos.toByteArray();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return yourBytes;
    }

    public static Object bytes2object(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInput in = null;
        Object o;
        try {
            in = new ObjectInputStream(bis);
            o = in.readObject();
        } finally {
            try {
                bis.close();
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return o;
    }


    public static byte[] convert( DataHandler file ) {

        try {

            // file data:
            //   1) create byte[] buffer
            //   2) write content into it
            //   3) copy the buffer as a new array
            final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            file.writeTo( buffer );
            return buffer.toByteArray();

        } catch ( IOException e ) {
            System.err.println( "File conversion failed. " + e.getMessage() );
            throw new RuntimeException( e );
        }
    }

    public static DataHandler convert( byte[] file, String name ) {

        return new ByteArrayDataHandler( file, name );
    }

    public static DataHandler loadAsHandler( String name ) {

        try {
            return convert( loadAsBytes( name ), name );
        } catch ( Exception e ) {
            System.err.println( "File loading failed. " + e.getMessage() );
            throw new RuntimeException( e );
        }
    }

    public static byte[] loadAsBytes( String name ) {

        InputStream input = null;

        try {

            // open file input stream
            // allocate proper buffer
            // load it
            // close the stream
            input = Thread.currentThread().getContextClassLoader().getResourceAsStream( name );
            byte[] data = new byte[ input.available() ];
            input.read( data );
            return data;

        } catch ( IOException e ) {
            System.err.println( "File loading failed. " + e.getMessage() );
            throw new RuntimeException( e );
        } finally {
            try {
                if ( input != null ) input.close();
            } catch ( IOException e ) {}
        }
    }
}

class ByteArrayDataHandler extends DataHandler {

    public ByteArrayDataHandler( byte[] data, String name ) {
        super( new ByteArrayDataSource( data, name ) );
    }
}

class ByteArrayDataSource implements DataSource {

    private InputStream is;

    private String contentType;

    private String name;

    public ByteArrayDataSource( byte[] array, String name ) {
        this.is = new ByteArrayInputStream( array );
        this.contentType = "application/octet-stream";
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public InputStream getInputStream() throws IOException {
        return is;
    }

    public String getName() {
        return name;
    }

    public OutputStream getOutputStream() throws IOException {
        throw new UnsupportedOperationException();
    }
}
