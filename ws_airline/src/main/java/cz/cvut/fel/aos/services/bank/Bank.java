package cz.cvut.fel.aos.services.bank;

import cz.cvut.fel.aos.services.bank.secured.BankService;
import cz.cvut.fel.aos.services.bank.secured.SecuredService;
import cz.cvut.fel.aos.services.bank.secured.TransactionException;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.handler.WSHandlerConstants;

import javax.xml.namespace.QName;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class Bank {

    private static final QName SERVICE_NAME = new QName( "http://centralbank.org/", "SecuredService" );
    private static final URL WSDL_URL = SecuredService.WSDL_LOCATION;

    public static BankService createSecuredService() {

        SecuredService factory = new SecuredService( WSDL_URL, SERVICE_NAME );
        BankService service = factory.getSecuredPort();

        // konfigurace vstupniho filtru
        Map<String, Object> inContext = new HashMap<String, Object>();
        inContext.put( WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE + " " + WSHandlerConstants.ENCRYPT ); // akce, kterou registrujeme
        inContext.put( WSHandlerConstants.SIG_PROP_FILE, "bank.properties" ); // konfigurace keystore
        inContext.put( WSHandlerConstants.SIGNATURE_USER, "bank" ); // kterou identitu z keystore budeme pouzivat
        inContext.put( WSHandlerConstants.DEC_PROP_FILE, "client.properties" ); // konfigurace keystore
        inContext.put( WSHandlerConstants.ENCRYPTION_USER, "client" ); // kterou identitu z keystore budeme pouzivat
        inContext.put( WSHandlerConstants.PW_CALLBACK_CLASS, "cz.cvut.fel.aos.services.bank.PasswordCallback" ); // prihlaseni ke keystore
        registerInInterceptor( service, inContext );

        // konfigurace vystupniho filtru
        Map<String, Object> outContext = new HashMap<String, Object>();
        outContext.put( WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE + " " + WSHandlerConstants.ENCRYPT ); // akce, kterou registrujeme
        outContext.put( WSHandlerConstants.USER, "xxx" ); // povinna konfigurace, ale nepouzivame ji ted
        outContext.put( WSHandlerConstants.SIG_PROP_FILE, "client.properties" ); // konfigurace keystore
        outContext.put( WSHandlerConstants.SIGNATURE_USER, "client" ); // kterou identitu z keystore budeme pouzivat
        outContext.put( WSHandlerConstants.ENC_PROP_FILE, "bank.properties" ); // konfigurace keystore
        outContext.put( WSHandlerConstants.ENCRYPTION_USER, "bank" ); // kterou identitu z keystore budeme pouzivat
        outContext.put( WSHandlerConstants.PW_CALLBACK_CLASS, "cz.cvut.fel.aos.services.bank.PasswordCallback" ); // prihlaseni ke keystore
        registerOutInterceptor( service, outContext );

        return service;
    }

    protected static void registerInInterceptor( Object service, Map<String, Object> context ) {
        // filtr pro obsluhu WS-Security
        WSS4JInInterceptor interceptor = new WSS4JInInterceptor( context );

        // na ziskaneho klienta zaregistrujeme vstupni filtr, ktery se bude aplikovat na vsechny prichozi zpravy bez vyjimek
        getClient( service ).getInInterceptors().add( interceptor );

        // na ziskaneho klienta zaregistrujeme vstupni filtr, ktery se bude aplikovat na vsechny prichozi chybove zpravy
        getClient( service ).getInFaultInterceptors().add( interceptor );
    }

    protected static void registerOutInterceptor( Object service, Map<String, Object> context ) {
        // filtr pro obsluhu WS-Security
        WSS4JOutInterceptor interceptor = new WSS4JOutInterceptor( context );

        // na ziskaneho klienta zaregistrujeme vystupni filtr, ktery se bude aplikovat na odchozi zpravy
        getClient( service ).getOutInterceptors().add( interceptor );
    }

    private static Client getClient( Object service ) {
        // musime ziskat klienta vzdalene sluzby, abychom mu mohli pridat dalsi konfiguraci
        return ( (ClientProxy) Proxy.getInvocationHandler(service) ).getClient();
    }

}
