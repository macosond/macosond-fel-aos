package cz.cvut.fel.aos.resources.mapping;


import cz.cvut.fel.aos.db.entities.*;

import java.util.*;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

//DB -> XML(JSON) adapter
@XmlRootElement(name="flight")
public class MappingFlight {

    private long id;
    private Date dateOfDeparture;
    private double distance;
    private long seats;
    private String name;
    private double price;
    private long fromId;
    private long toId;
    private String uri;

    public MappingFlight(){}

    public MappingFlight(Flight flight){
        if(flight != null){
            this.id = flight.getId();
            this.dateOfDeparture = flight.getDateOfDeparture();
            this.distance = flight.getDistance();
            this.seats = flight.getSeats();
            this.name = flight.getName();
            this.price = flight.getPrice();
            this.fromId = flight.getFromId();
            this.toId = flight.getToId();
        }
    }

    public MappingFlight(Flight flight, UriBuilder ub){
        this(flight);
        this.uri = ub.path(""+this.id).build().getPath();
    }

    @XmlElement(required=true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateOfDeparture() {
        return  dateOfDeparture;
    }

    @XmlElement(name="dateOfdeparture")
    @XmlJavaTypeAdapter(DateSerializer.class)
    public void setDateOfDeparture(Date date) {
        dateOfDeparture = date;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getSeats() {
        return seats;
    }

    public void setSeats(long s) {
        seats = s;
    }

    @XmlElement(required=true)
    public String getName() {
        return name;
    }

    public void setName(String n) {
        name = n;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @XmlElement(name="from")
    public long getFromId() {
        return fromId;
    }

    public void setFromId(long d) {
        fromId = d;
    }

    @XmlElement(name="to")
    public long getToId() {
        return toId;
    }

    public void setTo(long d) {
        toId = d;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return this.uri;
    }

}