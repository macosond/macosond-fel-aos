package cz.cvut.fel.aos.db.entities;

import cz.cvut.fel.aos.resources.mapping.MappingDestination;

import java.util.*;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class Destination {

    private long id;
    private String name;
    private double latitude;
    private double longitude;

    public Destination() {}

    public Destination(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Destination(MappingDestination mappingDestination) {
        if (mappingDestination != null) {
            this.id         = mappingDestination.getId();
            this.name       = mappingDestination.getName();
            this.latitude   = mappingDestination.getLatitude();
            this.longitude  = mappingDestination.getLongitude();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
