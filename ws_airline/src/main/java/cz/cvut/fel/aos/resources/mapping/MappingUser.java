package cz.cvut.fel.aos.resources.mapping;

import cz.cvut.fel.aos.db.entities.User;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//DB -> XML(JSON) adapter
@XmlRootElement(name="user")
public class MappingUser {
    
    private String nickname;
    private String password;
    private List<String> emails;
    private String uri;
    
    public MappingUser(){}

    public MappingUser(User user) {
        if(user != null){
            this.nickname = user.getNick();
            this.password = user.getPass();
            this.emails   = new LinkedList<String>(user.getEmails());
        }
    }

    public MappingUser(User user, UriBuilder ub) {
        this(user);
        this.uri = ub.path(this.nickname).build().getPath();
    }

    @XmlElement(required=true)
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public List<String> getEmails() {
        if (emails == null) {
            return new ArrayList<String>();
        }
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
    
    public void setUri(String uri) {
        this.uri = uri;
    }
    
    public String getUri() {
        return this.uri;
    }
    
}
