
package cz.cvut.fel.aos.services.printer.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cz.cvut.fel.aos.services.printer.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PrinterError_QNAME = new QName("http://airlineprinter.org/", "PrinterError");
    private final static QName _Print_QNAME = new QName("http://airlineprinter.org/", "print");
    private final static QName _PrintResponse_QNAME = new QName("http://airlineprinter.org/", "printResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cz.cvut.fel.aos.services.printer.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrinterError }
     * 
     */
    public PrinterError createPrinterError() {
        return new PrinterError();
    }

    /**
     * Create an instance of {@link PrintResponse }
     * 
     */
    public PrintResponse createPrintResponse() {
        return new PrintResponse();
    }

    /**
     * Create an instance of {@link Print }
     * 
     */
    public Print createPrint() {
        return new Print();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrinterError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://airlineprinter.org/", name = "PrinterError")
    public JAXBElement<PrinterError> createPrinterError(PrinterError value) {
        return new JAXBElement<PrinterError>(_PrinterError_QNAME, PrinterError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Print }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://airlineprinter.org/", name = "print")
    public JAXBElement<Print> createPrint(Print value) {
        return new JAXBElement<Print>(_Print_QNAME, Print.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrintResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://airlineprinter.org/", name = "printResponse")
    public JAXBElement<PrintResponse> createPrintResponse(PrintResponse value) {
        return new JAXBElement<PrintResponse>(_PrintResponse_QNAME, PrintResponse.class, null, value);
    }

}
