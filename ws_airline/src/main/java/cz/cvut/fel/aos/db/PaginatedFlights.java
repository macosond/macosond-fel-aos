package cz.cvut.fel.aos.db;

import cz.cvut.fel.aos.db.entities.Flight;

import java.util.Collection;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class PaginatedFlights {
    public long maxFlights;
    public Collection<Flight> flights;

    public PaginatedFlights(Collection<Flight> flights, long maxFlights) {
        this.flights = flights;
        this.maxFlights = maxFlights;
    }
}
