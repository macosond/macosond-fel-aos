package cz.cvut.fel.aos.services.jms;

import cz.cvut.fel.aos.db.entities.Reservation;
import cz.cvut.fel.aos.services.jms.client.*;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;

import java.io.IOException;
import java.net.URL;
import javax.activation.DataHandler;
import static cz.cvut.fel.aos.services.jms.FileUtils.convert;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class EmailSenderClient {

        private static final QName SERVICE_NAME = new QName( "http://airlinesender.org/", "SenderService" );

        public static void main( String args[] ) throws IOException {

            Sender printer = createService();
            Reservation r = new Reservation();
            r.setId(80);
            try {
                printer.sendEmail(convert(FileUtils.object2byte(r), "reservation"), "ondra@macoszek.cz");
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        public static Sender createService() {
            URL wsdlURL = SenderService.WSDL_LOCATION;

            SenderService provider = new SenderService( wsdlURL, SERVICE_NAME );
            Sender sender = provider.getSenderPort();

            // set up MTOM support DataHandler
            BindingProvider bindingProvider = ( BindingProvider ) sender;
            SOAPBinding binding = ( SOAPBinding ) bindingProvider.getBinding();
            binding.setMTOMEnabled( true );

            return sender;
        }

    }

