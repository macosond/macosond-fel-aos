
package cz.cvut.fel.aos.services.jms.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cz.cvut.fel.aos.services.jms.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SenderError_QNAME = new QName("http://airlinesender.org/", "SenderError");
    private final static QName _SendEmail_QNAME = new QName("http://airlinesender.org/", "sendEmail");
    private final static QName _SendEmailResponse_QNAME = new QName("http://airlinesender.org/", "sendEmailResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cz.cvut.fel.aos.services.jms.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SenderError }
     * 
     */
    public SenderError createSenderError() {
        return new SenderError();
    }

    /**
     * Create an instance of {@link SendEmailResponse }
     * 
     */
    public SendEmailResponse createSendEmailResponse() {
        return new SendEmailResponse();
    }

    /**
     * Create an instance of {@link SendEmail }
     * 
     */
    public SendEmail createSendEmail() {
        return new SendEmail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SenderError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://airlinesender.org/", name = "SenderError")
    public JAXBElement<SenderError> createSenderError(SenderError value) {
        return new JAXBElement<SenderError>(_SenderError_QNAME, SenderError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://airlinesender.org/", name = "sendEmail")
    public JAXBElement<SendEmail> createSendEmail(SendEmail value) {
        return new JAXBElement<SendEmail>(_SendEmail_QNAME, SendEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://airlinesender.org/", name = "sendEmailResponse")
    public JAXBElement<SendEmailResponse> createSendEmailResponse(SendEmailResponse value) {
        return new JAXBElement<SendEmailResponse>(_SendEmailResponse_QNAME, SendEmailResponse.class, null, value);
    }

}
