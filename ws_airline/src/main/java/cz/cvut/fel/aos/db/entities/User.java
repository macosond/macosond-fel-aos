package cz.cvut.fel.aos.db.entities;

import java.util.Collection;
import java.util.LinkedList;
import javax.xml.bind.annotation.XmlRootElement;
import cz.cvut.fel.aos.resources.mapping.*;

/**
 * Class that simulates DB entity.
 */
public class User {

    private String nick;
    private String pass;
    private Collection<String> emails = new LinkedList<String>();

    public User() {}
    
    public User(MappingUser mappingUser) {
        if (mappingUser != null) {
            this.nick   = mappingUser.getNickname();
            this.pass   = mappingUser.getPassword();
            this.emails = mappingUser.getEmails();
        }
    }

    public String getNick() {
        return nick;
    }

    public String getPass() {
        return pass;
    }

    public Collection<String> getEmails() {
        return emails;
    }
    
    public void addEmail(String email){
        this.emails.add(email);
    }
    
}
