package cz.cvut.fel.aos.db;

import cz.cvut.fel.aos.db.entities.*;
import cz.cvut.fel.aos.exceptions.BadRequestException;
import cz.cvut.fel.aos.resources.mapping.DateSerializer;
import cz.cvut.fel.aos.resources.mapping.MappingFlight;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import javax.ws.rs.HeaderParam;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Class that simulates in-memory database.
 */
public class DB {

    public static DB shared = new DB();

    private long sharedMaxId = 1;

    private Map<String, User> usersByNickName = new HashMap<String, User>();
    private Map<Long, Flight> flightsById = new HashMap<Long, Flight>();
    private Map<String, Flight> flightsByName = new HashMap<String, Flight>();
    private Map<Long, Reservation> reservationsById = new HashMap<Long, Reservation>();
    private Map<Long, Destination> destinationsById = new HashMap<Long, Destination>();
    private Map<String, Destination> destinationsByName = new HashMap<String, Destination>();

    public DB() {
        Destination london = new Destination("London", 51.508742, -0.006866);
        addDestination(london);
        Destination paris = new Destination("Paris", 48.835797, 2.454071);
        addDestination(paris);
        Destination prague = new Destination("Prague", 50.085344, 14.433181);
        addDestination(prague);
        Destination rome = new Destination("Rome", 41.902277, 12.5036);
        addDestination(rome);

        try {
            Flight londonToParis = new Flight("London to Paris", DateSerializer.toDate("2014-02-10T10:00:00+01:00"), 400, 100, 12000, london.getId(), paris.getId());
            addFlight(londonToParis);
            Flight parisToLondon = new Flight("Paris to London", DateSerializer.toDate("2014-02-15T10:00:00+01:00"), 400, 100, 12000, paris.getId(), london.getId());
            addFlight(parisToLondon);
            Flight parisToPrague = new Flight("Paris to Prague", DateSerializer.toDate("2014-02-20T10:00:00+01:00"), 400, 100, 12000, paris.getId(), prague.getId());
            addFlight(parisToPrague);
            Flight parisToPrague2 = new Flight("Paris to Prague 2", DateSerializer.toDate("2014-03-20T10:00:00+01:00"), 400, 100, 12000, paris.getId(), prague.getId());
            addFlight(parisToPrague2);
            Flight parisToPrague3 = new Flight("Paris to Prague 3", DateSerializer.toDate("2014-04-20T10:00:00+01:00"), 400, 100, 12000, paris.getId(), prague.getId());
            addFlight(parisToPrague3);
            Flight parisToPrague4 = new Flight("Paris to Prague 4", DateSerializer.toDate("2014-05-20T10:00:00+01:00"), 400, 100, 12000, paris.getId(), prague.getId());
            addFlight(parisToPrague4);
        } catch (ParseException e) {
            System.out.println("Parse error during filling db with data");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    // ------------------------------------------------------------------------
    //  Users
    // ------------------------------------------------------------------------

    public boolean addUser(User user){
        String nick = user.getNick();
        if(usersByNickName.containsKey(nick)) {
            return false;
        }
        usersByNickName.put(nick, user);
        return true;
    }
    
    public User getUserByNick(String nick){
        return usersByNickName.get(nick);
    }
    
    public AOSMemoryDBUserQueryset getAllUsers(){
        return new AOSMemoryDBUserQueryset(usersByNickName.values());
    }

    // ------------------------------------------------------------------------
    //  Flights
    // ------------------------------------------------------------------------

    public void addFlight(Flight flight) throws Exception {

        flight.setId(sharedMaxId++);
        Long id = new Long(flight.getId());
        if (flightsById.containsKey(id) || flightsByName.containsKey(flight.getName())) {
            throw new BadRequestException("Flight cannot be created");
        }
        flightsById.put(id,flight);
        flightsByName.put(flight.getName(), flight);

    }

    public void renameFlight(Flight entity, String newName) {
        flightsByName.remove(entity.getName());
        entity.setName(newName);
        flightsByName.put(newName, entity);
    }

    public void updateFlight(Flight flight) throws Exception{

        Long id = new Long(flight.getId());
        if (!flightsById.containsKey(id) || !flightsByName.containsKey(flight.getName())) {
            throw new Exception("Cannot update flight");
        }
        flightsById.put(id,flight);
        flightsByName.put(flight.getName(), flight);

    }

    public void removeFlight(Flight flight) {
        flightsById.remove(flight.getId());
        flightsByName.remove(flight.getName());
        for (Reservation reservation : flight.reservations) {
            reservationsById.remove(reservation);
        }
    }

    public Flight getFlightById(long id) {
        return flightsById.get(new Long(id));
    }

    public Flight getFlightByName(String name) {
        return flightsByName.get(name);
    }

    public Collection<Flight> getAllFlights() {
        List<Flight> flights = new ArrayList<Flight>(flightsById.values());
        Collections.sort(flights, new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                long f1 = o1.getId();
                long f2 = o2.getId();
                if (f1 == f2) {
                    return 0;
                } else if (f1 < f2) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
        return flights;
    }


    public PaginatedFlights getAllFlights(String filter, String order, String base, String offset) throws Exception {

        Date from = null;
        Date to = null;
        if (filter != null && !filter.isEmpty() && filter.indexOf(",") != -1) {
            filter = filter.trim();
            for (String line : filter.split(",")) {
                if (line.indexOf("=") == -1) { continue; };
                String[]tmp = line.split("=");
                if (tmp.length != 2) { continue; }
                String name = tmp[0];
                String value = tmp[1];
                if (name.equals("dateOfDepartureFrom")) {
                    try {
                        from = DateSerializer.toDate(value);
                        System.out.println(from);
                    } catch (ParseException e) {
                        throw new Exception("Cannot parse dateOfDepartureFrom argument");
                    }
                } else if (name.equals("dateOfDepartureTo")) {
                    try {
                        to = DateSerializer.toDate(value);
                        System.out.println(to);
                    } catch (ParseException e) {
                        throw new Exception("Cannot parse dateOfDepartureTo argument");
                    }
                }
            }
        }


        List<Flight> flights = new ArrayList<Flight>(flightsById.values());
        if (to != null && from != null) {
            System.out.println("filtering out by date");
            // filter out not wanted items
            List<Flight> toRemove = new ArrayList<Flight>();
            for(Flight flight : flights) {
                if (flight.getDateOfDeparture().compareTo(from) == 1
                 && flight.getDateOfDeparture().compareTo(to) == -1) {
                    System.out.println("keep "+flight);
                } else {
                    toRemove.add(flight);
                    System.out.println("remove "+flight);
                }
            }
            flights.removeAll(toRemove);
        }

        FlightComparator comparator = new FlightComparator();
        if (order != null && !order.isEmpty() && order.indexOf(':') != -1) {
            String[]tmp = order.split(":");
            if (!tmp[0].equals("name")) {
                comparator.nameOrdering= false;
            }
            if (!tmp[1].equals("asc")) {
                comparator.ascOrdering= false;
            }
        }

        Collections.sort(flights, comparator);

        int pagesPerPage = 15;
        int pagingOffset = 0;
        if (base != null && !base.isEmpty()) {
            pagesPerPage = Integer.parseInt(base);
        }
        if (offset != null && !offset.isEmpty()) {
            pagingOffset = Integer.parseInt(offset);
        }
        List<Flight> paginatedFlights = new ArrayList<Flight>(pagesPerPage);
        for (int i = pagingOffset; i < pagingOffset+pagesPerPage; i++) {
            if (i >= flights.size()) {
                break;
            }
            paginatedFlights.add(flights.get(i));
        }

        return new PaginatedFlights(paginatedFlights, flights.size());
    }

    class FlightComparator implements Comparator<Flight> {

        public boolean nameOrdering = true;
        public boolean ascOrdering = true;

        @Override
        public int compare(Flight f1, Flight f2) {
            Comparable o1 = null;
            Comparable o2 = null;
            if (nameOrdering) {
                o1 = f1.getName();
                o2 = f2.getName();
            } else {
                o1 = f1.getDateOfDeparture();
                o2 = f2.getDateOfDeparture();
            }

            int r = 0;
            if (o1 == null && o2 == null) {
                r = 0;
            } if (o1 == null) {
                r = -1;
            } else if (o2 == null) {
                r = 1;
            } else {
                r = o1.compareTo(o2);
            }

            if (!ascOrdering) {
                r = (r == 0 ? 0 : (r > 0 ? -1 : 1));
            }

            return r;
        }
    }

    // ------------------------------------------------------------------------
    //  Reservations
    // ------------------------------------------------------------------------

    public void addReservation(Reservation reservation) throws Exception {
        reservation.setId(sharedMaxId++);
        reservation.setCreated(new Date());
        reservation.setPassword(randomString(12));
        reservation.setState("new");

        if (reservation.getSeats() <= 0) {
            throw new Exception("Cannot find make reservation with zero seats");
        }

        if (getFlightById(reservation.getFlightId()) == null) {
            throw new Exception("Cannot find flight with given id");
        }
        Flight flight = getFlightById(reservation.getFlightId());
        if (flight.availableSeats()-reservation.getSeats() < 0) {
            throw new Exception("Cannot book more seats than is available");
        }

        flight.reservations.add(reservation);

        Long id = new Long(reservation.getId());
        if (reservationsById.containsKey(id)) {
            throw new Exception("Reservation cannot be created");
        }
        reservationsById.put(id, reservation);
    }

    public Reservation getReservationById(long id) {
        return reservationsById.get(new Long(id));
    }

    public void payReservation(Reservation reservation) throws Exception {
        if (!reservation.getState().equals("new")) {
            throw new Exception("Reservation must be in 'new' state in order to pay it");
        }
        reservation.setState("paid");
    }

    public boolean isReservationPaid(Reservation reservation) {
        return reservation.getState().equals("paid");
    }

    public void cancelReservation(Reservation reservation) {
        reservation.setState("canceled");
    }

    public void removeReservation(Reservation reservation) {
        reservationsById.remove(reservation.getId());
        Flight flight = getFlightById(reservation.getFlightId());
        flight.reservations.remove(reservation);
    }

    public Collection<Reservation> getAllReservations() {
        List<Reservation> reservations = new ArrayList<Reservation>(reservationsById.values());
        Collections.sort(reservations, new Comparator<Reservation>() {
            @Override
            public int compare(Reservation o1, Reservation o2) {
                long f1 = o1.getId();
                long f2 = o2.getId();
                if (f1 == f2) {
                    return 0;
                } else if (f1 < f2) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
        return reservations;
    }

    // ------------------------------------------------------------------------
    //  Destinations
    // ------------------------------------------------------------------------

    public boolean addDestination(Destination destination) {
        destination.setId(sharedMaxId++);
        Long id = new Long(destination.getId());
        if (destinationsById.containsKey(id) || destinationsByName.containsKey(destination.getName())) {
            return false;
        }
        destinationsById.put(id, destination);
        destinationsByName.put(destination.getName(), destination);
        return true;
    }

    public boolean renameDestination(Destination entity, String newName) {
        destinationsByName.remove(entity.getName());
        entity.setName(newName);
        destinationsByName.put(newName, entity);
        return true;
    }

    public boolean updateDestination(Destination updatedEntity) {
        Long id = new Long(updatedEntity.getId());
        if (!destinationsById.containsKey(id) || !destinationsByName.containsKey(updatedEntity.getName())) {
            return false;
        }
        destinationsById.put(id,updatedEntity);
        destinationsByName.put(updatedEntity.getName(), updatedEntity);
        return true;
    }

    public Destination getDestinationById(long id) {
        return destinationsById.get(new Long(id));
    }

    public Destination getDestinationByName(String name) {
        return destinationsByName.get(name);
    }

    public void removeDestination(Destination e) {
        destinationsById.remove(e.getId());
        destinationsByName.remove(e.getName());
    }

    public Collection<Destination> getAllDestinations() {
        List<Destination> destinations = new ArrayList<Destination>(destinationsById.values());
        Collections.sort(destinations, new Comparator<Destination>() {
            @Override
            public int compare(Destination o1, Destination o2) {
                long f1 = o1.getId();
                long f2 = o2.getId();
                if (f1 == f2) {
                    return 0;
                } else if (f1 < f2) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });
        return destinations;
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static Random rnd = new Random();

    // Source: http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
    public String randomString(int len) {
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

}
