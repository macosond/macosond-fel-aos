package cz.cvut.fel.aos.services.printer;

import cz.cvut.fel.aos.db.entities.Reservation;
import cz.cvut.fel.aos.services.printer.client.*;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;

import java.io.IOException;
import java.net.URL;
import javax.activation.DataHandler;

import static cz.cvut.fel.aos.services.printer.FileUtils.convert;


/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class PrinterClient {

    private static final QName SERVICE_NAME = new QName( "http://airlineprinter.org/", "PrinterService" );

    public static void main( String args[] ) throws IOException {

        Printer printer = createService();
        Reservation r = new Reservation();
        r.setFlightId(80);

        try {
            DataHandler ticket = printer.print(FileUtils.convert(FileUtils.object2byte(r), "reservation"));
            System.out.println(new String(FileUtils.convert(ticket)));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static Printer createService() {
        URL wsdlURL = PrinterService.WSDL_LOCATION;

        PrinterService provider = new PrinterService( wsdlURL, SERVICE_NAME );
        Printer printer = provider.getPrinterPort();

        // set up MTOM support DataHandler
        BindingProvider bindingProvider = ( BindingProvider ) printer;
        SOAPBinding binding = ( SOAPBinding ) bindingProvider.getBinding();
        binding.setMTOMEnabled( true );

        return printer;
    }

}
