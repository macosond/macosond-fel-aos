package cz.cvut.fel.aos.db.entities;

import cz.cvut.fel.aos.resources.mapping.*;

import java.util.*;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class Flight {

    private long id;
    private String name;
    private Date dateOfDeparture;
    private double distance;
    private long seats;
    private double price;
    private long fromId;
    private long toId;

    public HashSet<Reservation> reservations = new HashSet<Reservation>();

    public Flight() {}

    public Flight(String name, Date dateOfDeparture, double distance, long seats, double price, long fromId, long toId) {
        this.name = name;
        this.dateOfDeparture = dateOfDeparture;
        this.distance = distance;
        this.seats = seats;
        this.price = price;
        this.fromId = fromId;
        this.toId = toId;
    }

    public Flight(MappingFlight mappingFlight) {
        if (mappingFlight != null) {
            this.id         = mappingFlight.getId();
            this.name       = mappingFlight.getName();
            this.price      = mappingFlight.getPrice();
            this.seats      = mappingFlight.getSeats();
            this.dateOfDeparture = mappingFlight.getDateOfDeparture();
            this.distance   = mappingFlight.getDistance();
            this.fromId     = mappingFlight.getFromId();
            this.toId       = mappingFlight.getToId();
        }
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(Date date) {
        dateOfDeparture = date;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getSeats() {
        return seats;
    }

    public void setSeats(long seats) {
        this.seats = seats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getFromId() {
        return fromId;
    }

    public void setFromId(long e) {
        fromId = e;
    }

    public long getToId() {
        return toId;
    }

    public void setToId(long e) {
        toId = e;
    }

    public long availableSeats() {
        long available = seats;
        for (Reservation reservation : reservations) {
            available -= reservation.getSeats();
        }
        return available > 0 ? available : 0;
    }

    @Override
    public String toString() {
        return name;
    }
}

