package cz.cvut.fel.aos.resources.mapping;

import cz.cvut.fel.aos.db.entities.*;

import java.util.*;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
@XmlRootElement(name="reservation")
public class MappingReservation {

    private long id;
    private long seats;
    private String password;
    private Date created;
    private String state;
    private String uri;
    private long flightId;

    public MappingReservation(){}

    public MappingReservation(Reservation reservation) {
        if (reservation != null) {
            this.id = reservation.getId();
            this.seats = reservation.getSeats();
            this.password = reservation.getPassword();
            this.created = reservation.getCreated();
            this.state = reservation.getState();
            this.flightId = reservation.getFlightId();
        }
    }

    public MappingReservation(Reservation reservation, UriBuilder ub) {
        this(reservation);
        this.uri = ub.path(""+this.id).build().getPath();
    }

    @XmlElement(required=true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSeats() {
        return seats;
    }

    public void setSeats(long s) {
        seats = s;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String p) {
        password = p;
    }

    @XmlJavaTypeAdapter(DateSerializer.class)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date date) {
        created = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String s) {
        state = s;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return this.uri;
    }

    public long getFlightId() {
        return flightId;
    }

    public void setFlightId(long flightId) {
        this.flightId = flightId;
    }

}
