'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('airline', [
  'ngRoute',
  'airline.filters',
  'airline.services',
  'airline.directives',
  'airline.controllers'
]);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {templateUrl: 'static/partials/Home.html', controller: 'Home'});
  $routeProvider.when('/logout', {templateUrl: 'static/partials/Home.html', controller: 'Logout'});
  $routeProvider.when('/destination', {templateUrl: 'static/partials/DestinationList.html', controller: 'DestinationList'});
  $routeProvider.when('/destination/add-new', {templateUrl: 'static/partials/DestinationAddNew.html', controller: 'DestinationAddNew'});
  $routeProvider.when('/destination/:id', {templateUrl: 'static/partials/DestinationUpdate.html', controller: 'DestinationUpdate'});
  $routeProvider.when('/flight', {templateUrl: 'static/partials/FlightList.html', controller: 'FlightList'});
  $routeProvider.when('/flight/add-new', {templateUrl: 'static/partials/FlightAddNew.html', controller: 'FlightAddNew'});
  $routeProvider.when('/flight/:id', {templateUrl: 'static/partials/FlightUpdate.html', controller: 'FlightUpdate'});
  $routeProvider.when('/reservation', {templateUrl: 'static/partials/ReservationList.html', controller: 'ReservationList'});
  $routeProvider.when('/reservation/detail/:id', {templateUrl: 'static/partials/ReservationDetail.html', controller: 'ReservationDetail'});
  $routeProvider.when('/reservation/add-new', {templateUrl: 'static/partials/ReservationAddNew.html', controller: 'ReservationAddNew'});
  $routeProvider.otherwise({redirectTo: '/'});
}]);
