
echo "----------------------------------------"
echo " Home "
echo "----------------------------------------"


echo ">> WADL"
curl -i -H "Content-type: application/json"  \
	-X GET "http://localhost:8080/airline/application.wadl"
echo ""
echo "----------------------------------------"

echo ">> List"
curl -H "Content-type: application/json"  \
	-X GET "http://localhost:8080/airline/" | python -m json.tool
echo ""
echo "----------------------------------------"
