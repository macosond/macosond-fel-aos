package cz.cvut.fel.aos.services.jms.mdb;

import java.util.logging.Logger;

import cz.cvut.fel.aos.services.jms.MessageWrapper;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

@MessageDriven(name = "EmailQueueMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/EmailMDBQueue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class EmailQueueMDB implements MessageListener {

	private final static Logger LOGGER = Logger.getLogger(EmailQueueMDB.class.toString());

	public void onMessage(Message rcvMessage) {
		try {
			try {
				Thread.sleep(5000);             
			} catch(InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
			if (rcvMessage instanceof TextMessage) {
				TextMessage msg = (TextMessage) rcvMessage;
				LOGGER.info("Received Message from queue: " + msg.getText());
			} else if (rcvMessage instanceof ObjectMessage) {
				ObjectMessage msg = (ObjectMessage) rcvMessage;
				MessageWrapper wraper = (MessageWrapper) msg.getObject();
				LOGGER.info("Received Message from queue: " + wraper.toString());
			} else {
				LOGGER.warning("Message of wrong type: " + rcvMessage.getClass().getName());
			}
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}
}
