package cz.cvut.fel.aos.services.jms.server;


import javax.activation.DataHandler;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlMimeType;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
@WebService( targetNamespace = "http://airlinesender.org/" )
public interface Sender {

    @WebMethod
    void sendEmail(
            @XmlMimeType( "application/octet-stream" )
            @WebParam( name = "file" ) DataHandler file,
            @WebParam( name = "email" ) String email
    ) throws SenderException;

}
