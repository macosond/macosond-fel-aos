package cz.cvut.fel.aos.services.jms.server;

import cz.cvut.fel.aos.services.jms.MessageWrapper;
import cz.cvut.fel.aos.db.entities.Reservation;
import cz.cvut.fel.aos.services.jms.FileUtils;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.jms.*;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import static cz.cvut.fel.aos.services.jms.FileUtils.convert;
import static cz.cvut.fel.aos.services.jms.FileUtils.object2byte;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

//@MTOM
@WebService(
        portName = "SenderPort",
        serviceName = "SenderService",
        targetNamespace = "http://airlinesender.org/",
        endpointInterface = "cz.cvut.fel.aos.services.jms.server.Sender"
)
public class SenderService implements Sender {

    @Resource(mappedName = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/queue/EmailMDBQueue")
    private Queue queue;

    @Override
    public void sendEmail( DataHandler file, String email ) throws SenderException {

        String name = file.getName();
        byte[] bytes = convert(file);
        Reservation reservation = null;
        DataHandler printedFile;

        try {
            reservation = (Reservation) FileUtils.bytes2object(bytes);

            System.out.println("Reservation with ID " + reservation.getId() + " will be send to email "+email);

            Connection connection = null;
            try {
                Destination destination = queue;
                System.out.println("creating connection");
                connection = connectionFactory.createConnection();
                System.out.println("creating session");
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                System.out.println("creating producer");
                MessageProducer messageProducer = session.createProducer(destination);

                // settings
                messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
                messageProducer.setTimeToLive(10000); // miliseconds to live
                connection.start();

                // send object message
                ObjectMessage objectMessage = session.createObjectMessage();
                objectMessage.setObject(new MessageWrapper(reservation, email));
                System.out.println("sending message from producer");
                messageProducer.send(objectMessage);
                System.out.println("message sent");

            } catch (JMSException e) {
                System.out.println("JMSE >> "+ e.getMessage());
                throw e;
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (JMSException e) {
                        System.out.println("JMSE 2 >> "+e.getMessage());
                        throw e;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new SenderException("Printing failed "+e.getMessage());
        }

    }

}
