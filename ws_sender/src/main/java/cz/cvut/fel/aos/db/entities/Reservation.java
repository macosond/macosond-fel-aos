package cz.cvut.fel.aos.db.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
public class Reservation implements Serializable {

    static final long serialVersionUID = 42L;

    private long id;
    private long seats;
    private String password;
    private Date created;
    private String state;
    private long flightId;

    public Reservation() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSeats() {
        return seats;
    }

    public void setSeats(long s) {
        seats = s;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String p) {
        password = p;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date d) {
        created = d;
    }

    public String getState() {
        return state;
    }

    public void setState(String s) {
        state = s;
    }

    public long getFlightId() {
        return flightId;
    }

    public void setFlightId(long f) {
        flightId = f;
    }

}
