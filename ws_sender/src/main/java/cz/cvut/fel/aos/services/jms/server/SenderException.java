package cz.cvut.fel.aos.services.jms.server;

import javax.xml.ws.WebFault;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

@WebFault( name = "SenderError", targetNamespace = "http://airlinesender.org/" )
public class SenderException extends Exception {

    public SenderException(String message) {
        super( message );
    }
}