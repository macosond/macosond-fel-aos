package cz.cvut.fel.aos.services.printer.server;


import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlMimeType;

import java.util.List;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */
@WebService( targetNamespace = "http://airlineprinter.org/" )
public interface Printer {

    @WebMethod
    @XmlMimeType( "application/octet-stream" )
    DataHandler print(
            @XmlMimeType( "application/octet-stream" )
            @WebParam( name = "file" ) DataHandler file
    ) throws PrinterException;

}
