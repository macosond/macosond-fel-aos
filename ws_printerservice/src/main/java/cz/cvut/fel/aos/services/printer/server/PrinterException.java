package cz.cvut.fel.aos.services.printer.server;

import javax.xml.ws.WebFault;

/**
 * CVUT / AOS / Ondrej Macoszek <macosond@fel.cvut.cz>
 */

@WebFault( name = "PrinterError", targetNamespace = "http://airlineprinter.org/" )
public class PrinterException extends Exception {

    public PrinterException( String message ) {
        super( message );
    }
}